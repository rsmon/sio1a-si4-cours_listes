package programmes;
import java.util.Scanner;

public class Lanceur {
         
    public static void main(String[] args) {
   
        String Titre_Programme1="1) Liste d'entiers simples";
        String Titre_Programme2="2) Liste d'entiers aléatoires  de longueur aleatoire";
        String Titre_Programme3="3) Liste de physiciens";
        String Titre_Programme4="4) Liste d'entiers";
        
        Programme1 p1= new Programme1();
        Programme2 p2= new Programme2();
        Programme3 p3= new Programme3();
        Programme4 p4= new Programme4();
        
        Scanner  clavier= new Scanner(System.in);
        
        int choix=0;
        
        do{
            
            System.out.println(Titre_Programme1);
            System.out.println(Titre_Programme2);
            System.out.println(Titre_Programme3);
            System.out.println(Titre_Programme4);
            System.out.println();
        
            System.out.print("Programme à lancer ( tapez 0 pour arrêter) ? "); 
            choix=clavier.nextInt();
    
            System.out.println();
            
            switch (choix)
               
            { 
                case 1: Programme1.main(null);break;
                case 2: Programme2.main(null);break; 
                case 3: Programme3.main(null);break; 
                case 4: Programme4.main(null);break;       
                     
            }  
       
            System.out.println();
            
         }while ( choix > 0);
    }
}




