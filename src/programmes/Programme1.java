package programmes;

import java.util.LinkedList;
import java.util.List;

public class Programme1 {
    
    public static void main(String[] args) {
  
        // Déclaration et création d'une liste vide
        
        List< Integer> listeDEntiers= new LinkedList<Integer>();
        
        // Ajout d'entiers dans la liste

        listeDEntiers.add(34);
        listeDEntiers.add(12);
        listeDEntiers.add(70);
        listeDEntiers.add(22);
        listeDEntiers.add(56);
        
        //Parcours de la liste
        
        System.out.println();
        
        for(Integer element : listeDEntiers ){
        
            System.out.print(element+ " ");
        }
        
        System.out.println("\n\n");    
    }
}


