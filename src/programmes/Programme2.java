package programmes;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Programme2 {
   
    
     public static void main(String[] args) {
    
        List< Integer> listeDEntiers= new LinkedList<Integer>();
    
        Random aleat= new Random();
                  
        // tirage au sort de la longueur de la liste entre 0 et 20
        Integer longueurListe=aleat.nextInt(21);
        
        for(int i=0;i<longueurListe;i++){
          
            // tirage au sort de la valeur (entre 1 et 100)de l'élément à ajouter
            Integer nombre =aleat.nextInt(100)+1;
            
            // Ajout du nombre dans la liste
            listeDEntiers.add(nombre);
        }
  
        //tri de la liste en ordre croissant
        Collections.sort(listeDEntiers);
        
        //parcours de la liste pour afficher ses éléments
        
        for(Integer v: listeDEntiers ){
        
            System.out.print(v+" ");
        }
        System.out.println();
          
        // recherche de l'élément de valeur 50
        
        if(listeDEntiers.contains(50)){
           
            System.out.println("\n50 se trouve dans la liste\n");
            System.out.print("D'ailleurs il se trouve à l'index: "+listeDEntiers.indexOf(50));
            System.out.println("\n ( Rappel: le premier indice est l'indice 0 )\n");
        }
        else{
           
            System.out.println("\n50 ne se trouve pas dans la liste\n");
        }
    }
}

