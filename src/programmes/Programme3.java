package programmes;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Programme3 {
   
    static List< String> listeDeNoms= new LinkedList<String>();
   
    public static void main(String[] args) {
        
        remplirLaListe();
           
        System.out.println("LISTE INITIALE\n");
        afficherLaListe();
        
        Collections.sort(listeDeNoms);
        
        System.out.println("LISTE TRIEE\n");
        afficherLaListe();     
   }
     
   static  void afficherLaListe(){
        
        int indice=0;
        for(String nom : listeDeNoms){
           
            System.out.printf("%2d %-15s\n",indice,nom); 
            indice++;
        }
  
        System.out.println();
        
       
    }

   static void remplirLaListe() {
        
        listeDeNoms.add("Einstein");listeDeNoms.add("Newton");listeDeNoms.add("Plank");
        listeDeNoms.add("Heisenberg");listeDeNoms.add("Curie");listeDeNoms.add("Gauss");
        listeDeNoms.add("Maxwell");listeDeNoms.add("Ampère");listeDeNoms.add("Fermi");
        listeDeNoms.add("Dirac");listeDeNoms.add("Tesla");listeDeNoms.add("Huygens");
        listeDeNoms.add("Kepler");listeDeNoms.add("Poincaré");listeDeNoms.add("Faraday");
        listeDeNoms.add("Boltzmann");listeDeNoms.add("Rutherford");listeDeNoms.add("Lavoisier");
        listeDeNoms.add("Dalton");listeDeNoms.add("Copernic");listeDeNoms.add("Kelvin");
        listeDeNoms.add("Feynman");listeDeNoms.add("Galilée");listeDeNoms.add("Meitneir");
    }    
}

