package programmes;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Programme4 {
   
    static Random         genAleatoire  = new Random();
    
    static List< Integer> listeDEntiers = new LinkedList<Integer>();
  
    public static void main(String[] args) {
    
        remplirLaListe();        
         
        System.out.println("LISTE NON TRIEE\n");
        afficherLaListe();
        
        System.out.println("A l'indice  18 se trouve l'entier: "+listeDEntiers.get(18));
        System.out.println();

        System.out.println("Minimum: "+Collections.min(listeDEntiers));
        System.out.println("Maximum: "+Collections.max(listeDEntiers));
        System.out.println();  
        
        
        Scanner clavier= new Scanner(System.in); 
        int     nombreSaisi;
     
        int index=0;
        
        System.out.println("Saisir un nombre qui se trouve ou non dans la liste");
        nombreSaisi=clavier.nextInt();
        
        if( listeDEntiers.contains(nombreSaisi) ){
        
            index = listeDEntiers.indexOf(nombreSaisi);
            System.out.println("Le nombre "+ nombreSaisi+ " se trouve à l'indice "+index);
            
        }  
        else{
        
            System.out.println("Le nombre "+ nombreSaisi+ " ne se trouve pas dans la liste\n");
            
        }   
        
        
//////////////////////////////////////////////////////////////////////////////////////////////////                
// TRI DE LA LISTE PAR ORDRE CROISSANT      
//////////////////////////////////////////////////////////////////////////////////////////////////        
        
        Collections.sort(listeDEntiers);
        
        System.out.println("LISTE TRIEE\n");
        afficherLaListe();
        
        
        System.out.println("Saisir un nombre qui se trouve ou non dans la liste");
        nombreSaisi=clavier.nextInt();
        
        if( listeDEntiers.contains(nombreSaisi) ){
        
            index = listeDEntiers.indexOf(nombreSaisi);
            System.out.println("Le nombre "+ nombreSaisi+ " se trouve à l'indice "+index);
            
        }  
        else{
        
            System.out.println("Le nombre "+ nombreSaisi+ " ne se trouve pas dans la liste\n");
            
        }     
    }
     
    
   // FONCTIONS 
    
    static void remplirLaListe() {
        
        listeDEntiers.clear();
        
        for(int i=0; i<20; i++){
          
            listeDEntiers.add(genAleatoire.nextInt(1000));
        }    
    } 
    
    static void afficherLaListe(){
        
        int index=0;
        for(Integer val : listeDEntiers){
           
            System.out.printf("%2d %15d\n",index,val); 
            index++;
        }
        
        System.out.println(); 
    }
 
   // FIN FONCTIONS  
    
}




